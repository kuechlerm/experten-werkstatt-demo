export class SpecialBindings {

    titel = 'Special Bindings';

    name = '';
    names = [];

    get nameZuLang() {
        return this.name.length > 3;
    }

    add() {
        this.names.push(this.name);
    }
}