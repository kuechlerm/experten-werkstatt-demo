import { inject, customAttribute, DOM } from 'aurelia-framework';

// @customAttribute('wichtig')
@inject(DOM.Element)
export class WichtigCustomAttribute {

    constructor(private element: HTMLElement) {
    }

    valueChanged(newValue, oldValue) {

        this.element.style.color = newValue;
    }
}