
import * as moment from 'moment';

export class DateFormatValueConverter {

    toView(value: Date, format: string) {

        if (!value) return '';

        return moment(value).format(format);
    }
}