import { autoinject } from 'aurelia-framework';
import { RouterConfiguration, Router } from 'aurelia-router';

export class App {

    router: Router;

    configureRouter(config: RouterConfiguration, router: Router) {

        config.title = 'experten.werkstatt';
        config.map([

            { route: '', name: 'board', moduleId: './board/board', nav: false, title: 'Board' },
            { route: 'stringInterpolation', name: 'stringInterpolation', moduleId: './stringInterpolation/stringInterpolation', nav: true, title: 'String Interpolation', settings: { thema: 'vertikal' } },
            { route: 'dataBinding', name: 'dataBinding', moduleId: './dataBinding/dataBinding', nav: true, title: 'Data Binding', settings: { thema: 'vertikal' } },
            { route: 'eventBinding', name: 'eventBinding', moduleId: './eventBinding/eventBinding', nav: true, title: 'Event Binding', settings: { thema: 'vertikal' } },
            { route: 'specialBindings', name: 'specialBindings', moduleId: './specialBindings/specialBindings', nav: true, title: 'Special Bindings', settings: { thema: 'vertikal' } },
            { route: 'customAttributes', name: 'customAttributes', moduleId: './customAttributes/customAttributes', nav: true, title: 'Custom Attributes', settings: { thema: 'vertikal' } },
            { route: 'valueConverters', name: 'valueConverters', moduleId: './valueConverters/valueConverters', nav: true, title: 'Value Converters', settings: { thema: 'vertikal' } },
            { route: 'bindingBehave', name: 'bindingBehave', moduleId: './bindingBehave/bindingBehave', nav: true, title: 'Binding Behaviours', settings: { thema: 'vertikal' } },

            { route: 'viewOnly', name: 'viewOnly', moduleId: './viewOnly/viewOnly', nav: true, title: 'Require View Only', settings: { thema: 'horizontal' } },
            { route: 'component', name: 'component', moduleId: './component/component', nav: true, title: 'Component', settings: { thema: 'horizontal' } },
            { route: 'slots', name: 'slots', moduleId: './slots/slots', nav: true, title: 'Slots', settings: { thema: 'horizontal' } },
            { route: 'dynamicComposition', name: 'dynamicComposition', moduleId: './dynamicComposition/dynamicComposition', nav: true, title: 'Dynamic Composition', settings: { thema: 'horizontal' } },

            { route: 'router', name: 'router', moduleId: './router/router', nav: true, title: 'Router', settings: { thema: 'horizontal' } },
            { route: 'router/:parameter', href:'router', name: 'routerWithParam', moduleId: './router/router', nav: false, title: 'Router with Param', settings: { thema: 'horizontal' } },
            { route: 'events', name: 'events', moduleId: './events/events', nav: true, title: 'Events', settings: { thema: 'horizontal' } }

        ]);

        this.router = router;
    }
}
