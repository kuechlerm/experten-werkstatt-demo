
import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';

@autoinject
export class Sender {

    text;

    constructor(private events: EventAggregator) {
    }

    senden() {
        this.events.publish('sender', { text: this.text });
    }

}