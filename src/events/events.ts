
import { autoinject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';

@autoinject
export class Events {

    text;

    constructor(private events: EventAggregator) {

        events.subscribe('sender', data => this.text = data.text);
    }

}