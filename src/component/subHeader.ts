import { bindable } from 'aurelia-framework';

export class SubHeader {

    @bindable title = 'SubHeader';

    titleChanged(newValue) {

        console.log('titleChanged', newValue);
    }

}