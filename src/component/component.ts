
import { EinService } from './einService';
import { autoinject } from 'aurelia-framework';

@autoinject
export class Component {

    info = '';

    constructor(einService: EinService) {
        this.info = einService.getInfo();
    }

    attached() {
        console.log('DOM Ready');
    }

}