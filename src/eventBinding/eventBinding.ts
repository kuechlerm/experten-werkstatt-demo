export class Router {

    titel = 'Event Binding';

    name = '';

    gruss() {
        alert(this.name);
    }

    wheel(event) {
        console.log('WHEEL', event);
    }

}