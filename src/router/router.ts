export class Router {

    kannWeg = false;

    parameter = '';

    activate(params) {

        this.parameter = params.parameter;

        return new Promise((resolve, reject) => {

            setTimeout(() => {
                console.log('Router fertig');
                resolve();
            }, 3000);
        });

    }

    canDeactivate() {

        return this.kannWeg;
    }

}