
export class StringInterpolation {

    titel = 'String Interpolation';
    name = '';

    get gruss() {
        return `Hallo ${this.name}!`;
    }

    get nameZuLang(){
        return this.name.length > 3;
    }
}