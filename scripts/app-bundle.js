define('app',["require", "exports"], function (require, exports) {
    "use strict";
    var App = (function () {
        function App() {
        }
        App.prototype.configureRouter = function (config, router) {
            config.title = 'experten.werkstatt';
            config.map([
                { route: '', name: 'board', moduleId: './board/board', nav: false, title: 'Board' },
                { route: 'stringInterpolation', name: 'stringInterpolation', moduleId: './stringInterpolation/stringInterpolation', nav: true, title: 'String Interpolation', settings: { thema: 'vertikal' } },
                { route: 'dataBinding', name: 'dataBinding', moduleId: './dataBinding/dataBinding', nav: true, title: 'Data Binding', settings: { thema: 'vertikal' } },
                { route: 'eventBinding', name: 'eventBinding', moduleId: './eventBinding/eventBinding', nav: true, title: 'Event Binding', settings: { thema: 'vertikal' } },
                { route: 'specialBindings', name: 'specialBindings', moduleId: './specialBindings/specialBindings', nav: true, title: 'Special Bindings', settings: { thema: 'vertikal' } },
                { route: 'customAttributes', name: 'customAttributes', moduleId: './customAttributes/customAttributes', nav: true, title: 'Custom Attributes', settings: { thema: 'vertikal' } },
                { route: 'valueConverters', name: 'valueConverters', moduleId: './valueConverters/valueConverters', nav: true, title: 'Value Converters', settings: { thema: 'vertikal' } },
                { route: 'bindingBehave', name: 'bindingBehave', moduleId: './bindingBehave/bindingBehave', nav: true, title: 'Binding Behaviours', settings: { thema: 'vertikal' } },
                { route: 'viewOnly', name: 'viewOnly', moduleId: './viewOnly/viewOnly', nav: true, title: 'Require View Only', settings: { thema: 'horizontal' } },
                { route: 'component', name: 'component', moduleId: './component/component', nav: true, title: 'Component', settings: { thema: 'horizontal' } },
                { route: 'slots', name: 'slots', moduleId: './slots/slots', nav: true, title: 'Slots', settings: { thema: 'horizontal' } },
                { route: 'dynamicComposition', name: 'dynamicComposition', moduleId: './dynamicComposition/dynamicComposition', nav: true, title: 'Dynamic Composition', settings: { thema: 'horizontal' } },
                { route: 'router', name: 'router', moduleId: './router/router', nav: true, title: 'Router', settings: { thema: 'horizontal' } },
                { route: 'router/:parameter', href: 'router', name: 'routerWithParam', moduleId: './router/router', nav: false, title: 'Router with Param', settings: { thema: 'horizontal' } },
                { route: 'events', name: 'events', moduleId: './events/events', nav: true, title: 'Events', settings: { thema: 'horizontal' } }
            ]);
            this.router = router;
        };
        return App;
    }());
    exports.App = App;
});

define('environment',["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = {
        debug: true,
        testing: true
    };
});

define('main',["require", "exports", './environment'], function (require, exports, environment_1) {
    "use strict";
    Promise.config({
        warnings: {
            wForgottenReturn: false
        }
    });
    function configure(aurelia) {
        aurelia.use
            .standardConfiguration();
        if (environment_1.default.debug) {
            aurelia.use.developmentLogging();
        }
        if (environment_1.default.testing) {
            aurelia.use.plugin('aurelia-testing');
        }
        aurelia.start().then(function () { return aurelia.setRoot(); });
    }
    exports.configure = configure;
});

define('bindingBehave/bindingBehave',["require", "exports"], function (require, exports) {
    "use strict";
    var BindingBehave = (function () {
        function BindingBehave() {
        }
        return BindingBehave;
    }());
    exports.BindingBehave = BindingBehave;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('board/board',["require", "exports", 'aurelia-framework', 'aurelia-router'], function (require, exports, aurelia_framework_1, aurelia_router_1) {
    "use strict";
    var Board = (function () {
        function Board(router) {
            this.router = router;
        }
        Board = __decorate([
            aurelia_framework_1.autoinject, 
            __metadata('design:paramtypes', [aurelia_router_1.Router])
        ], Board);
        return Board;
    }());
    exports.Board = Board;
});

define('component/einService',["require", "exports"], function (require, exports) {
    "use strict";
    var EinService = (function () {
        function EinService() {
        }
        EinService.prototype.getInfo = function () {
            return 'Ein Service - Info';
        };
        return EinService;
    }());
    exports.EinService = EinService;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('component/component',["require", "exports", './einService', 'aurelia-framework'], function (require, exports, einService_1, aurelia_framework_1) {
    "use strict";
    var Component = (function () {
        function Component(einService) {
            this.info = '';
            this.info = einService.getInfo();
        }
        Component.prototype.attached = function () {
            console.log('DOM Ready');
        };
        Component = __decorate([
            aurelia_framework_1.autoinject, 
            __metadata('design:paramtypes', [einService_1.EinService])
        ], Component);
        return Component;
    }());
    exports.Component = Component;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('component/subHeader',["require", "exports", 'aurelia-framework'], function (require, exports, aurelia_framework_1) {
    "use strict";
    var SubHeader = (function () {
        function SubHeader() {
            this.title = 'SubHeader';
        }
        SubHeader.prototype.titleChanged = function (newValue) {
            console.log('titleChanged', newValue);
        };
        __decorate([
            aurelia_framework_1.bindable, 
            __metadata('design:type', Object)
        ], SubHeader.prototype, "title", void 0);
        return SubHeader;
    }());
    exports.SubHeader = SubHeader;
});

define('customAttributes/customAttributes',["require", "exports"], function (require, exports) {
    "use strict";
    var CustomAttributes = (function () {
        function CustomAttributes() {
            this.color = 'black';
        }
        return CustomAttributes;
    }());
    exports.CustomAttributes = CustomAttributes;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('customAttributes/wichtigCustomAttribute',["require", "exports", 'aurelia-framework'], function (require, exports, aurelia_framework_1) {
    "use strict";
    var WichtigCustomAttribute = (function () {
        function WichtigCustomAttribute(element) {
            this.element = element;
        }
        WichtigCustomAttribute.prototype.valueChanged = function (newValue, oldValue) {
            this.element.style.color = newValue;
        };
        WichtigCustomAttribute = __decorate([
            aurelia_framework_1.inject(aurelia_framework_1.DOM.Element), 
            __metadata('design:paramtypes', [HTMLElement])
        ], WichtigCustomAttribute);
        return WichtigCustomAttribute;
    }());
    exports.WichtigCustomAttribute = WichtigCustomAttribute;
});

define('dataBinding/dataBinding',["require", "exports"], function (require, exports) {
    "use strict";
    var DataBinding = (function () {
        function DataBinding() {
            this.titel = 'Data Binding';
            this.name = '';
        }
        Object.defineProperty(DataBinding.prototype, "nameZuLang", {
            get: function () {
                return this.name.length > 3;
            },
            enumerable: true,
            configurable: true
        });
        return DataBinding;
    }());
    exports.DataBinding = DataBinding;
});

define('dynamicComposition/dynamicComposition',["require", "exports"], function (require, exports) {
    "use strict";
    var DynamicComposition = (function () {
        function DynamicComposition() {
            this.data = {};
        }
        return DynamicComposition;
    }());
    exports.DynamicComposition = DynamicComposition;
});

define('eventBinding/eventBinding',["require", "exports"], function (require, exports) {
    "use strict";
    var Router = (function () {
        function Router() {
            this.titel = 'Event Binding';
            this.name = '';
        }
        Router.prototype.gruss = function () {
            alert(this.name);
        };
        Router.prototype.wheel = function (event) {
            console.log('WHEEL', event);
        };
        return Router;
    }());
    exports.Router = Router;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('events/events',["require", "exports", 'aurelia-framework', 'aurelia-event-aggregator'], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1) {
    "use strict";
    var Events = (function () {
        function Events(events) {
            var _this = this;
            this.events = events;
            events.subscribe('sender', function (data) { return _this.text = data.text; });
        }
        Events = __decorate([
            aurelia_framework_1.autoinject, 
            __metadata('design:paramtypes', [aurelia_event_aggregator_1.EventAggregator])
        ], Events);
        return Events;
    }());
    exports.Events = Events;
});

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
define('events/sender',["require", "exports", 'aurelia-framework', 'aurelia-event-aggregator'], function (require, exports, aurelia_framework_1, aurelia_event_aggregator_1) {
    "use strict";
    var Sender = (function () {
        function Sender(events) {
            this.events = events;
        }
        Sender.prototype.senden = function () {
            this.events.publish('sender', { text: this.text });
        };
        Sender = __decorate([
            aurelia_framework_1.autoinject, 
            __metadata('design:paramtypes', [aurelia_event_aggregator_1.EventAggregator])
        ], Sender);
        return Sender;
    }());
    exports.Sender = Sender;
});

define('router/router',["require", "exports"], function (require, exports) {
    "use strict";
    var Router = (function () {
        function Router() {
            this.kannWeg = false;
            this.parameter = '';
        }
        Router.prototype.activate = function (params) {
            this.parameter = params.parameter;
            return new Promise(function (resolve, reject) {
                setTimeout(function () {
                    console.log('Router fertig');
                    resolve();
                }, 3000);
            });
        };
        Router.prototype.canDeactivate = function () {
            return this.kannWeg;
        };
        return Router;
    }());
    exports.Router = Router;
});

define('slots/einElement',["require", "exports"], function (require, exports) {
    "use strict";
    var EinElement = (function () {
        function EinElement() {
        }
        return EinElement;
    }());
    exports.EinElement = EinElement;
});

define('slots/slots',["require", "exports"], function (require, exports) {
    "use strict";
    var DataBinding = (function () {
        function DataBinding() {
            this.titel = 'Slots';
        }
        return DataBinding;
    }());
    exports.DataBinding = DataBinding;
});

define('specialBindings/specialBindings',["require", "exports"], function (require, exports) {
    "use strict";
    var SpecialBindings = (function () {
        function SpecialBindings() {
            this.titel = 'Special Bindings';
            this.name = '';
            this.names = [];
        }
        Object.defineProperty(SpecialBindings.prototype, "nameZuLang", {
            get: function () {
                return this.name.length > 3;
            },
            enumerable: true,
            configurable: true
        });
        SpecialBindings.prototype.add = function () {
            this.names.push(this.name);
        };
        return SpecialBindings;
    }());
    exports.SpecialBindings = SpecialBindings;
});

define('stringInterpolation/stringInterpolation',["require", "exports"], function (require, exports) {
    "use strict";
    var StringInterpolation = (function () {
        function StringInterpolation() {
            this.titel = 'String Interpolation';
            this.name = '';
        }
        Object.defineProperty(StringInterpolation.prototype, "gruss", {
            get: function () {
                return "Hallo " + this.name + "!";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StringInterpolation.prototype, "nameZuLang", {
            get: function () {
                return this.name.length > 3;
            },
            enumerable: true,
            configurable: true
        });
        return StringInterpolation;
    }());
    exports.StringInterpolation = StringInterpolation;
});

define('testing/testing',["require", "exports"], function (require, exports) {
    "use strict";
    var Testing = (function () {
        function Testing() {
        }
        return Testing;
    }());
    exports.Testing = Testing;
});

define('valueConverters/dateFormat',["require", "exports", 'moment'], function (require, exports, moment) {
    "use strict";
    var DateFormatValueConverter = (function () {
        function DateFormatValueConverter() {
        }
        DateFormatValueConverter.prototype.toView = function (value, format) {
            if (!value)
                return '';
            return moment(value).format(format);
        };
        return DateFormatValueConverter;
    }());
    exports.DateFormatValueConverter = DateFormatValueConverter;
});

define('valueConverters/valueConverters',["require", "exports"], function (require, exports) {
    "use strict";
    var ValueConverters = (function () {
        function ValueConverters() {
            this.jetzt = new Date();
            this.format = 'DD.MM.YYYY';
        }
        return ValueConverters;
    }());
    exports.ValueConverters = ValueConverters;
});

define('viewOnly/viewOnly',["require", "exports"], function (require, exports) {
    "use strict";
    var ViewOnly = (function () {
        function ViewOnly() {
        }
        return ViewOnly;
    }());
    exports.ViewOnly = ViewOnly;
});

define('text!app.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"purecss/build/pure.css\"></require>\r\n    <require from=\"purecss/build/grids-responsive.css\"></require>\r\n\r\n    <require from=\"./app.css\"></require>\r\n\r\n    <div class=\"back\">\r\n        <a href=\"/#/\">${ router.isNavigating ? '~' : '<' }</a>\r\n    </div>\r\n\r\n    <div class=\"content\">\r\n        <router-view></router-view>\r\n    </div>\r\n\r\n</template>"; });
define('text!app.css', ['module'], function(module) { module.exports = ".content {\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 100vw;\r\n    height: 100vh;\r\n}\r\n\r\n.back a {\r\n    position: fixed;\r\n    text-decoration: none;\r\n    font-size: 3rem;\r\n}"; });
define('text!bindingBehave/bindingBehave.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <h1 wichtig.bind=\"color\">Binding Behaviours</h1>\r\n\r\n    <p>\r\n       Throttle: ${ text & throttle:1000 }\r\n    </p>\r\n\r\n    <p>\r\n       Debounce: ${ text & debounce:1000 }\r\n    </p>\r\n\r\n\r\n    <input type=\"text\" value.bind=\"text\">\r\n\r\n</template>"; });
define('text!board/board.css', ['module'], function(module) { module.exports = ".themen-container {\r\n    display: flex;\r\n    justify-content: space-around;\r\n    flex-wrap: wrap;\r\n    width: 100%;\r\n}\r\n\r\n.thema {\r\n    padding: 20px;\r\n    margin: 30px;\r\n    background: lightgray;\r\n    color: white;\r\n}\r\n\r\n.thema__link {\r\n    color: black;\r\n    text-decoration: none;\r\n}\r\n\r\n.thema--vertikal {\r\n    background: seagreen;\r\n}\r\n\r\n.thema--horizontal {\r\n    background: steelblue;\r\n}\r\n\r\n.thema--bonus {\r\n    background: orange;\r\n}"; });
define('text!board/board.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"./board.css\"></require>\r\n\r\n    <div class=\"themen-container\">\r\n        <div repeat.for=\"row of router.navigation\" class=\"thema thema--${row.settings.thema}\">\r\n            <a class=\"thema__link\" href.bind=\"row.href\">${row.title}</a>\r\n        </div>\r\n    </div>\r\n\r\n</template>"; });
define('text!stringInterpolation/stringInterpolation.css', ['module'], function(module) { module.exports = ".zu-lang {\r\n    background: red;\r\n}"; });
define('text!component/component.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"./subHeader\"></require>\r\n\r\n    <h1>Component</h1>\r\n\r\n    <sub-header title=\"Etwas Text\"></sub-header>\r\n\r\n    <p>${info}</p>\r\n\r\n</template>"; });
define('text!component/subHeader.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <h2>${title}</h2>\r\n\r\n</template>"; });
define('text!customAttributes/customAttributes.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"./wichtigCustomAttribute\"></require>\r\n\r\n    <h1 wichtig.bind=\"color\">Custom Attributes</h1>\r\n\r\n    <input type=\"text\" value.bind=\"color\">\r\n\r\n</template>"; });
define('text!dataBinding/dataBinding.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <h1 inner-text.bind=\"titel\"></h1>\r\n\r\n    <p innerhtml.one-time=\"name\"></p>\r\n\r\n    <input type=\"text\" value.two-way=\"name\">\r\n\r\n    <button type=\"button\" class=\"pure-button\"\r\n        disabled.bind=\"nameZuLang\"\r\n        title.bind=\"name\">\r\n        Hallo\r\n    </button>\r\n\r\n</template>"; });
define('text!dynamicComposition/dynamicComposition.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <compose view=\"component/component.html\" model.bind=\"data\"></compose>\r\n\r\n</template>"; });
define('text!eventBinding/eventBinding.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <h1 inner-text.bind=\"titel\"></h1>\r\n\r\n    <p innerhtml.bind=\"name\"></p>\r\n\r\n    <input type=\"text\" value.bind=\"name\">\r\n\r\n    <button type=\"button\" class=\"pure-button\"\r\n        click.delegate=\"gruss()\"\r\n        mousewheel.delegate=\"wheel($event)\">\r\n        Hallo\r\n    </button>\r\n\r\n</template>"; });
define('text!events/events.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"./sender\"></require>\r\n\r\n    <sender></sender>\r\n\r\n    <hr> Ziel: ${text}\r\n\r\n</template>"; });
define('text!events/sender.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <input type=\"text\" value.bind=\"text\">\r\n    <button type=\"button\" click.delegate=\"senden()\">Senden</button>\r\n\r\n</template>"; });
define('text!router/router.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <h1>Router</h1>\r\n\r\n    <h2>${parameter}</h2>\r\n\r\n    <input type=\"checkbox\" checked.bind=\"kannWeg\"> Kann weg\r\n\r\n</template>"; });
define('text!slots/einElement.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n<br>\r\n<h3>Ein Element</h3>\r\n\r\n<ul>\r\n    <li>\r\n        <slot></slot>\r\n    </li>\r\n</ul>\r\n\r\n</template>"; });
define('text!slots/slots.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"./einElement\"></require>\r\n\r\n    <h1>${titel}</h1>\r\n\r\n    <ein-element>\r\n        Mit meinem Text\r\n    </ein-element>\r\n\r\n\r\n</template>"; });
define('text!specialBindings/specialBindings.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <h1>${titel}</h1>\r\n\r\n    <p>${name}</p>\r\n\r\n    <input type=\"text\" value.bind=\"name\">\r\n\r\n    <!--show-->\r\n    <button type=\"button\" class=\"pure-button\"\r\n        show.bind=\"!nameZuLang\"\r\n        click.delegate=\"add()\">\r\n        Hallo\r\n    </button>\r\n\r\n    <ul>\r\n        <!--<li repeat.for=\"name of names\">${name}</li>-->\r\n        <li repeat.for=\"name of names\">${name} - Index: ${$index} - First: ${$first} - Last: ${$last} - Odd: ${$odd} - Even: ${$even}</li>\r\n    </ul>\r\n\r\n</template>"; });
define('text!stringInterpolation/stringInterpolation.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"./stringInterpolation.css\"></require>\r\n\r\n    <h1>${titel}</h1>\r\n\r\n    <p>Name: <input type=\"text\" value.bind=\"name\"></p>\r\n\r\n    <p class=\"${nameZuLang ? 'zu-lang': ''}\">${gruss}</p>\r\n\r\n</template>"; });
define('text!testing/testing.html', ['module'], function(module) { module.exports = "<template>\r\n    Testing\r\n</template>"; });
define('text!valueConverters/valueConverters.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"./dateFormat\"></require>\r\n\r\n    <h1>Value Converters</h1>\r\n\r\n    <p>\r\n        ${ jetzt | dateFormat:format }\r\n    </p>\r\n\r\n    <input type=\"text\" value.bind=\"format\">\r\n\r\n</template>"; });
define('text!viewOnly/aView.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <h1>Require View Only</h1>\r\n\r\n</template>"; });
define('text!viewOnly/viewOnly.html', ['module'], function(module) { module.exports = "<template>\r\n\r\n    <require from=\"./aView.html\" as=\"der-titel\"></require>\r\n\r\n    <der-titel></der-titel>\r\n\r\n</template>"; });
//# sourceMappingURL=app-bundle.js.map